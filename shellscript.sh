#!/bin/sh
JENKINS_HOME=/Users/Jenkins
JEN=/Users/santhirajesh/Downloads
cd $JEN/kafka-new
PATH=$PATH:$JENKINS_HOME:$JEN
export PATH
bin/zookeeper-server-start.sh config/zookeeper.properties &
bin/kafka-server-start.sh config/server.properties &
cd $JEN/elasticsearch-1.6.0
bin/elasticsearch &
cd $JEN/kibana-4.1.4-darwin-x64
./bin/kibana &
sleep 15
open http://localhost:5601/